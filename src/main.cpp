#include "lorawan.h"
#include "standby.h"
#include "mbed.h"

Serial pc(USBTX, USBRX);

void Error_Handler(char* filename, unsigned int line)
{
	pc.printf("Error in %s@%d !!\r\n", filename, line);
	while (1);
}

int main(void)
{	  
	pc.baud(115200);
	pc.printf("Booting...\r\n");
  standby_verification();
#if 1
	initLoraWan();
	pc.printf("LoraWan init done\r\n");
	loraWanAction(2);
  Radio.Sleep( );
#endif  
	pc.printf("Standby...\r\n");
	config_GPIO();
  // Reset All GPIO port
  RCC->IOPRSTR = 0x9F;
  RCC->IOPRSTR = 0x0;
	
	standby_mode(5 * 60);
}
